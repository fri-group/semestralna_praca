
#ifndef SEMKA_TIC_TAC_TOE_SERVER_MAIN_H
#define SEMKA_TIC_TAC_TOE_SERVER_MAIN_H

#endif //SEMKA_TIC_TAC_TOE_SERVER_MAIN_H

void initHraciePole();
void vykresli();
void odosliKlientovi(int kod);
int spravTah(int pozicia, int hrac);
int skontrolujPole();
void overVitaza();
void * hlavneVlakno(void * parametre);
void * klientSocketVlakno(void * parametre);