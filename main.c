#include <stdio.h>
#include <netinet/in.h>
#include <strings.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <ctype.h>

typedef struct hlavne {
    pthread_mutex_t * mut;
    pthread_cond_t * spravTahKlient;
    pthread_cond_t * spravTahServer;
} HLAVNE;

typedef struct klientSocket {
    pthread_mutex_t * mut;
    pthread_cond_t * spravTahKlient;
    pthread_cond_t * spravTahServer;
} KLIENTSOCKET;

int hraciaPlocha[25];
int beziHra = 0;
int tahServer = 1;
int vyhral = 0;

/// komunikacia
int sockfd, newsockfd;
char buffer[256];
int n;

void initHraciePole() {
    for (int i = 0; i < 25; ++i) {
        hraciaPlocha[i] = 0;
    }
}

void vykresli() {
    if (beziHra == 1) {
        for (int i = 0; i < 25; ++i) {
            if (i % 5 == 0) {
                printf("\n| ");
            }
            if (hraciaPlocha[i] == 1) {
                printf(" X | ");
            } else if (hraciaPlocha[i] == -1) {
                printf(" O | ");
            } else {
                if (i < 10) {
                    printf(" %d | ", i);
                } else {
                    printf("%d | ", i);
                }
            }
        }
    } else {    // zaverecny-bez pomocnych cisel
        for (int i = 0; i < 25; ++i) {
            if (i % 5 == 0) {
                printf("\n| ");
            }
            if (hraciaPlocha[i] == 1) {
                printf(" X | ");
            } else if (hraciaPlocha[i] == -1) {
                printf(" O | ");
            } else {
                printf("   | ");
            }
        }
    }
    printf("\n");
}

void odosliKlientovi(int kod) {
    if (kod != 0) {
        bzero(buffer, 256);
        sprintf(buffer, "%d", kod);
    }
    n = write(newsockfd, buffer, strlen(buffer));
    if (n < 0) {
        perror("Zapisovanie sa nepodarilo!!");
    }
}

int spravTah(int pozicia, int hrac) {
    if (hraciaPlocha[pozicia] == 0) {
        if (hrac == 1) {
            hraciaPlocha[pozicia] = 1;
        } else if (hrac == 2) {
            hraciaPlocha[pozicia] = -1;
        }
    } else {
        /// obsadene policko
        return 1;
    }
    return 0;
}

// return 1 - vyhral server; 2 - klient; 0 - zatial nikto
int skontrolujPole() {
    int pocetStlpcov = 5;
    int pocetRiadkov = 5;

    // riadky
    int sum = 0;
    for (int row = 0; row < pocetRiadkov; ++row) {
        for (int col = 0; col < pocetStlpcov; ++col) {
            sum += hraciaPlocha[row * pocetRiadkov + col];
        }
        if (sum == 5) {
            return 1;
        } else if (sum == -5) {
            return 2;
        }
        sum = 0;
    }

    // stlpce
    for (int col = 0; col < pocetStlpcov; ++col){
        for (int row = 0; row < pocetRiadkov; ++row) {
            sum += hraciaPlocha[col + row * pocetStlpcov];
        }
        if (sum == 5) {
            return 1;
        } else if (sum == -5) {
            return 2;
        }
        sum = 0;
    }

    // diagonala TL
    for (int col = 0; col < pocetStlpcov; ++col) {
        for (int row = 0; row < pocetRiadkov; ++row) {
            if (col == row) {
                sum += hraciaPlocha[col + row * pocetStlpcov];
            }
        }
    }
    if (sum == 5) {
        return 1;
    } else if (sum == -5) {
        return 2;
    }
    sum = 0;

    // diagonala TR
    for (int col = 0; col < pocetStlpcov; ++col) {
        for (int row = 0; row < pocetRiadkov; ++row) {
            if ((col + row * pocetStlpcov) % 4 == 0) {
                sum += hraciaPlocha[col + row * pocetStlpcov];
            }
        }
    }
    if (sum == 5) {
        return 1;
    } else if (sum == -5) {
        return 2;
    }

    // remiza - kontrola ci je este nejake policko == 0
    for (int i = 0; i < pocetRiadkov * pocetStlpcov; ++i) {
        if (hraciaPlocha[i] == 0) {
            return 0;
        }
    }
    printf("remiza\n");
    return 3; /// remiza, uz nie mozny ziaden tah
}

int overVitaza() {
    int hodnotaOverenia = skontrolujPole();

    if (hodnotaOverenia == 1) {
        vyhral = 1;
        beziHra = 0;

        int poslednaHodnota = atoi(buffer);
        odosliKlientovi(poslednaHodnota+60); // <60,84>: odosle sa cislo z int <0,24> + 60
        usleep(5000);
        odosliKlientovi(1); // vyhral server

        return 1;
    } else if (hodnotaOverenia == 2) {
        vyhral = 2;
        beziHra = 0;
        vykresli();
        odosliKlientovi(98);

        return 2;
    } else if (hodnotaOverenia == 3) {
        vyhral = 3; //remiza
        beziHra = 0;
        vykresli();
        if (tahServer == 0) {
            int poslednaHodnota = atoi(buffer);
            odosliKlientovi(poslednaHodnota+60);
            printf("odosielam server poz remiza\n");
        }
        usleep(5000);
        odosliKlientovi(3); // remiza
        printf("odosielam server poz remiza\n");

        return 3;
    }
    return 0;
}

void * hlavneVlakno(void * parametre) {
    HLAVNE * data = (HLAVNE *) parametre;

    initHraciePole();

    while (beziHra == 1) {
        pthread_mutex_lock(data->mut);
        while (tahServer == 0) {
            printf("\nČaká sa na ťah klienta.\n");
            pthread_cond_wait(data->spravTahServer, data->mut);
        }
        if (beziHra == 0) {
            break;
        }
        vykresli();
        tahServer = 0;
        int pokr = 0;
        while (pokr == 0) {
            printf("Zadaj pozíciu alebo príkaz: ");
            bzero(buffer, 256);
            fgets(buffer, 255, stdin);

            if(!(isdigit(buffer[0]))) {
                printf("Zadal si neplatný znak!\n");
                continue;
            }

            int prikaz = atoi(buffer);
            if (prikaz >= 0 && prikaz <= 24) {
                int pozicia = prikaz;
                int hodnotaTahu = spravTah(pozicia, 1); // ak 0 => volne policko, ak ine, tak obsadene policko
                if (hodnotaTahu == 0) {
                    if (overVitaza() == 0) {
                        odosliKlientovi(0); // posle klientovi svoju zadanu poziciu na aktualizovanie pola
                    }
                    pokr = 1;
                } else {
                    printf("Políčko je už obsadené. Zvoľ iné!\n");
                }
                vykresli();
            } else if (prikaz == 99) {
                /// koniec - // na klienta sa posle hodnota 99, klient odosle 99 na server pre ukoncenie vlakna, vlakno serv konci, klient sa vypne
                printf("Hra sa končí...\n");
                beziHra = 0;
                odosliKlientovi(0); // posle klientovi hodnotu 99
                break;
            } else if (prikaz == 95) {
                /// restart
                odosliKlientovi(0); // posle klientovi hodnotu 95
                initHraciePole();
                vykresli();
            } else {
                printf("Neplatný príkaz!\n");
            }
        }
        pthread_mutex_unlock(data->mut);
    }
    return NULL;
}

void * klientSocketVlakno(void * parametre) {
    KLIENTSOCKET * data = (KLIENTSOCKET *) parametre;

    while (beziHra == 1) {
        bzero(buffer, 256);
        n = read(newsockfd, buffer, 255);
        if (n < 0) {
            perror("Čítanie sa nepodarilo");
        }
        int prijataHodnota = atoi(buffer);

        if (prijataHodnota >= 0 && prijataHodnota <= 24) {
            // vyuziva sa 49
        } else if (prijataHodnota == 99) {
            // klient dava prikaz na ukoncenie hry, spojenia.. klient sa odpaja a vypina
            if (beziHra == 1) { // toto len detail aby to nevypisalo ak sa hra konci vyhrou/prehrou
                printf("Klient ukončil hru...\n");
            }
            odosliKlientovi(99);
            beziHra = 0;
            tahServer = 1;
            pthread_cond_signal(data->spravTahServer); // aby sa skoncilo aj hlavne vlakno
        } else if (prijataHodnota == 95) {
            // klient ziada o novu hru - restart
            printf("Klient požiadal o reštart.\n");
            initHraciePole();
            tahServer = 1;
            pthread_cond_signal(data->spravTahServer); // aby sa skoncilo aj hlavne vlakno
        } else if (prijataHodnota == 49) {
            pthread_mutex_lock(data->mut);
            // ziadost na overenie
            bzero(buffer, 256);
            n = read(newsockfd, buffer, 255); // prijatie pozicie - ziadost od klienta
            if (n < 0) {
                perror("Čítanie sa nepodarilo");
            }
            prijataHodnota = atoi(buffer);
            int pozicia = prijataHodnota;
            int vysledok = spravTah(pozicia, 2);

            if (vysledok == 0) {
                printf("\nKlient označil pozíciu %d. \n", pozicia);
                odosliKlientovi(50);

                overVitaza();

                usleep(5000); // aby sa chvilu pockalo kym sa odosle; ale mozno ani netreba
                tahServer = 1;
                pthread_cond_signal(data->spravTahServer);
            } else {
                odosliKlientovi(51);
            }
            pthread_mutex_unlock(data->mut);
        }
    }
    return NULL;
}

int main(int argc, char * argv[]) {
    printf("Server sa zapína a čaká na klienta...\n");
    socklen_t cli_len;
    struct sockaddr_in serv_adr, cli_addr;
    if ( argc < 2) {
        fprintf(stderr, "Použitie: %s port\n", argv[0]);
    }
    bzero((char*)&serv_adr,sizeof(serv_adr));
    serv_adr.sin_family=AF_INET;
    serv_adr.sin_addr.s_addr = INADDR_ANY;
    serv_adr.sin_port = htons(atoi(argv[1]));
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        perror("Socket sa nevytvoril!!");
        return 1;
    }
    if (bind(sockfd, (struct sockaddr*)&serv_adr,sizeof(serv_adr)) < 0) {
        perror("Bindovanie sa nepodarilo!!");
        return 2;
    }
    listen(sockfd, n);
    cli_len = sizeof(cli_addr);
    newsockfd = accept(sockfd, (struct sockaddr*)&cli_addr, &cli_len);
    if (newsockfd < 0) {
        perror("Akceptácia sa nepodarila");
        return 3;
    }

    beziHra = 1;

    printf("~~~~ Tic-tac-toe ~~~~\n");
    printf("~~~ Lenka Samcová ~~~\n");
    printf("\n");
    printf("Tvoj znak je X\n");
    printf("Označenie pozície: <0,24>\n");
    printf("Reštart: 95\n");
    printf("Koniec: 99\n");

    // vytvorenie vlakien
    pthread_t klientSocket;
    pthread_t hlavne;

    pthread_mutex_t mut;
    pthread_cond_t spravTahKlient, spravTahServer;
    pthread_mutex_init(&mut, NULL);
    pthread_cond_init(&spravTahKlient, NULL);
    pthread_cond_init(&spravTahServer, NULL);

    HLAVNE dataHlavneVlakno = {&mut, &spravTahKlient, &spravTahServer};
    KLIENTSOCKET dataKlientSocket = {&mut, &spravTahKlient, &spravTahServer};

    pthread_create(&hlavne, NULL, &hlavneVlakno, &dataHlavneVlakno);
    pthread_create(&klientSocket, NULL, &klientSocketVlakno, &dataKlientSocket);

    pthread_join(hlavne, NULL);
    pthread_join(klientSocket, NULL);

    if (vyhral == 1) {
        printf("Vyhral si!\n");
    } else if (vyhral == 2) {
        printf("Prehral si. Klient bol lepší!\n");
    } else if (vyhral == 3) {
        printf("Remiza! Už nie je možný ďalší ťah.\n");
    }

    // clean up
    pthread_cond_destroy(&spravTahKlient);
    pthread_cond_destroy(&spravTahServer);
    pthread_mutex_destroy(&mut);

    close(newsockfd);
    close(sockfd);
    return 0;
}
